<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Edit_Tab extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$fieldset = $form->addFieldset('banners_form', array(
			'legend' => 'Banners'
		));

		$fieldset->addField('title', 'text', array(
			'label' => 'Título',
			'class' => 'required-entry',
			'required' => TRUE,
			'name' => 'title'
		));

		if( Mage::registry('banners_data') ) $form->setValue(Mage::registry('banners_data')->getData());

		return parent::_prepareForm();
	}
}
?