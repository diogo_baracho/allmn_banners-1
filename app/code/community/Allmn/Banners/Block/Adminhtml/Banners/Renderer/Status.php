<?php
/**
 * 
 */
class Allmn_Banners_Block_Adminhtml_Banners_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	/**
	 * @method render
	 *
	 * Render desired field using rows ID
	 *
	 * @param row - Varien_Object
	 * @return html
	 */
	public function render( Varien_Object $row ){
		return $row->getStatus() ? '<strong>Habilitado</strong>' : '<strong>Desabilitado</strong>' ;
	}
}