<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct(){
		parent::__construct();
		$this->_objectId = 'id';
		$this->_blockGroup = 'allmnbanners';
		$this->_controller = 'adminhtml_banners';

		//echo $this->_blockGroup.'/'.$this->_controller.'_'.$this->_mode.'_form';

		$this->_formScripts[] = '
			function saveAndContinueEdit(){
				editForm.submit($("edit_form").action+"back/edit/");
			}
		';

		$this->_updateButton('save', 'label', Mage::helper('adminhtml')->__('Save'));
		$this->_updateButton('delete', 'label', Mage::helper('adminhtml')->__('Delete'));

		$this->_addButton('saveandcontinue', array(
			'label'   => Mage::helper('adminhtml')->__('Save and Continue Edit'),
			'onclick' => 'saveAndContinueEdit()',
			'class'   => 'save'
		), -100);
	}

	public function getHeaderText(){
		if( Mage::registry('banners_data') && Mage::registry('banners_data')->getId() ) return 'Editando: '.$this->escapeHtml(Mage::registry('banners_data')->getTitle()).'<br />';
		else return 'Criar';
	}
}
?>