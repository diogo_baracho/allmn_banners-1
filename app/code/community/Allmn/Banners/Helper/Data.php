<?php
/**
 * 
 */

class Allmn_Banners_Helper_Data extends Mage_Core_Helper_Abstract
{
	public  $template = '';
	protected $block = 'allmnbanners/banners';
	protected $format = 'default';

	public function getImageUrl($file = NULL){
		if( empty($file) ) return NULL;

		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'allmnbanners/'.$file;
	}

	public function loadSlider($id = NULL){
		$html = Mage::app()->getLayout()->createBlock($this->block);

		if( !empty($this->template) ) $html = $html->setTemplate($this->template);

		if( is_numeric($id) ) $html = $html->setId($id);
		else $html = $html = $html->setIdentifier($id);

		return $html->setFormat($this->format)->toHtml();
	}

	// @TODO: Write function description
	public function setTemplate($template = NULL){
		$this->template = $template;

		return $this;
	}

	public function setFormat($format = NULL){
		$this->format = $format;

		return $this;
	}
}