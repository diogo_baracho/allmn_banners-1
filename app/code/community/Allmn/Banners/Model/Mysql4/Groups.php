<?php
/**
 * 
 */
class Allmn_Banners_Model_Mysql4_Groups extends Mage_Core_Model_Mysql4_Abstract{
	/**
	 * @method __construct
	 *
	 * @return
	 */
	public function _construct(){
		$this->_init('allmnbanners/groups', 'group_id');
	}
}