<?php
/**
 * 
 */
class Allmn_Banners_Model_Mysql4_Banners_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	/**
	 * @method __construct
	 *
	 * @return
	 */
	public function _construct(){
		parent::_construct();
		$this->_init('allmnbanners/banners');
	}
}