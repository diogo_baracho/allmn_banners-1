<?php
/**
 */
class Allmn_Banners_Model_Status extends Mage_Core_Model_Abstract{
	/**
	 * @method toOptionArray
	 *
	 * Populates the array for initial use
	 *
	 * @return paises
	 */
	public function toOptionArray(){
		return array(
			array('value' => 0, 'label' => 'Desabilitado'),
			array('value' => 1, 'label' => 'Habilitado')
		);
	}
}